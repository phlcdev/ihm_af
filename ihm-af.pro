#-------------------------------------------------
#
# Project created by QtCreator 2013-03-06T10:33:37
#
#-------------------------------------------------

QT       += core gui

TARGET = ihm-af
TEMPLATE = app

include(3rdparty/qextserialport/src/qextserialport.pri)

INCLUDEPATH += /usr/local/qt/include/Qt/ /usr/local/qt/include/

SOURCES += main.cpp\
        mainwindow.cpp\
        3rdparty/qcustomplot/qcustomplot.cpp

HEADERS  += mainwindow.h\
            3rdparty/qcustomplot/qcustomplot.h

FORMS    += mainwindow.ui

CONFIG += qtestlib

QMAKE_POST_LINK += ./update.sh

RESOURCES += \
    imagens.qrc
