#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->label_status->setVisible(false);
    ui->label_9->setVisible(false);
    QString portName = "/dev/ttyUSB0";

    COMport = new QextSerialPort(portName, QextSerialPort::EventDriven);
    COMport->setBaudRate(BAUD9600);
    COMport->setFlowControl(FLOW_OFF);
    COMport->setParity(PAR_EVEN);
    COMport->setDataBits(DATA_7);
    COMport->setStopBits(STOP_1);
    COMport->setTimeout(500);
    COMport->open(QIODevice::ReadWrite);
    if(COMport->isOpen())
    {
        qDebug() <<  "Conectado via porta "+portName;
    }
    connect(COMport,SIGNAL(readyRead()),this,SLOT(read_serial()));
    timerCheck = new QTimer(this);
    timerStatus = new QTimer(this);
    connect(timerCheck,SIGNAL(timeout()),this,SLOT(timerCheck_timout()));
    connect(timerStatus,SIGNAL(timeout()),this,SLOT(timerStatus_timout()));
    modo=0;
}

void MainWindow::timerStatus_timout()
{
    ui->label_status->setVisible(false);
    timerStatus->stop();
}

MainWindow::~MainWindow()
{
    delete ui;
}

unsigned char MainWindow::LRC(unsigned char *auchMsg,unsigned short usDataLen)
{
        unsigned char uchLRC = 0 ;
        while (usDataLen--)
                uchLRC += *auchMsg++;
        return ((unsigned char)-uchLRC);
}

int MainWindow::BytesToInt(QByteArray byteArr)
{
    QDataStream ds(&byteArr,QIODevice::ReadOnly);
    ds.setByteOrder(QDataStream::LittleEndian);

    int ret;
    qint16 tmp;
    ds >> tmp;
    ret = tmp;

    return ret;
}

void MainWindow::read_serial()
{
    QByteArray temp = COMport->readLine(60);
    QString datain(temp.trimmed());
    //qDebug() << datain;
    if(modo==1 && datain.startsWith(":01010105F8"))
    {
        timerCheck->stop();
        modo=2;
        ReadDatatoPlot();
        return;
    }
    if(modo==2 && datain.startsWith(":010302"))
    {
        ponto_memoria--;
        impedancia[ponto_memoria] = datain.mid(7,4).toInt(0,16);
        if(ponto_memoria<=0)
        {
            ReadVars(0);
            return;
        }
    }
    if(modo==3 && datain.startsWith(":010302"))
    {
        ui->lineEdit_freq_res->setText(QString::number(datain.mid(7,4).toInt(0,16)));
        ReadVars(1);
        return;
    }
    if(modo==4 && datain.startsWith(":010302"))
    {
        ui->lineEdit_imp_min->setText(QString::number((datain.mid(7,4).toInt(0,16)*100)/impedancia_maxima));
        plotdata(ui->customPlot);
        return;
    }
}

void MainWindow::ReadVars(int cmd)
{
    if(cmd==0)
    {
        modo=3;
        COMport->write(":010311A4000146\r\n");
        QTest::qWait(100);
    }
    else if(cmd==1)
    {
        modo=4;
        COMport->write(":010311A8000142\r\n");
        QTest::qWait(100);
    }
}

void MainWindow::timerCheck_timout()
{
    COMport->write(":010108050001F0\r\n");
}

void MainWindow::on_pushButton_Liga_clicked()
{
    //liga
    freq_max = ui->lineEdit_freq_max->text().toInt();
    freq_min = ui->lineEdit_freq_min->text().toInt();
    if((freq_max-freq_min)>=700)
    {
        passo = (freq_max - freq_min) / 700;
    } else
    {
        if(freq_min>freq_max)
        {
            ui->label_status->setText("Freq. Min < Freq. Max");
            ui->label_status->setVisible(true);
            timerStatus->start(2500);
            return;
        }
        else
        {
            ui->label_status->setText("Range >= 700");
            ui->label_status->setVisible(true);
            timerStatus->start(2500);
            return;
        }
    }
    COMport->write(":0105080BFF00E8\r\n"); //desliga saida rf
    QTest::qWait(100);
    COMport->write(":0105080B0000E7\r\n");
    QTest::qWait(100);

    //freq_max = 20100;
    //freq_min = 19400;
    //passo = (freq_max - freq_min) / 700;
    if((freq_max-freq_min)%700!=0)
    {
        passo++;
    }
    qDebug() << passo;
    qDebug() << freq_min;
    qDebug() << freq_max;

    WriteData(freq_max,0x119A); // valor maximo
    QTest::qWait(100);
    WriteData(freq_min,0x119E); // valor minimo
    QTest::qWait(100);
    WriteData(passo,0x11A2); // valor passo
    QTest::qWait(100);
    //return;
    COMport->write(":0105080AFF00E9\r\n");
    QTest::qWait(100);
    COMport->write(":0105080A0000E8\r\n");
    timerCheck->start(1000);
    modo=1;
    ui->label_status->setText("Analisando ferramenta");
    ui->label_status->setVisible(true);
}

void MainWindow::WriteData(quint16 valor, quint16 Endereco)
{
    unsigned char lrc;
    QString Frame;
    quint16 h_slave_cmd = 0x0106;
    QByteArray req;

    QDataStream stream(&req, QIODevice::WriteOnly);
    stream << h_slave_cmd << Endereco << valor;
    lrc = LRC(reinterpret_cast<unsigned char*>(req.data()),6);

    stream << lrc;
    Frame.prepend(":");
    Frame.append(req.toHex().toUpper());
    QByteArray reqa;
    reqa.append(Frame);
    COMport->write(reqa+"\r\n");
    //qDebug() << reqa << "\r\n";
}

void MainWindow::RequestData(quint16 Endereco)
{
    unsigned char lrc;
    QString Frame;
    quint16 h_slave_cmd = 0x0103;
    quint16 ZeroF = 0x0001;
    QByteArray req;

    QDataStream stream(&req, QIODevice::WriteOnly);
    if(Endereco>4095)
    {
        Endereco = (Endereco - 4096) + 0x9000;
    }
    else
    {
        Endereco = Endereco + 0x1000;
    }
    stream << h_slave_cmd << Endereco << ZeroF;
    lrc = LRC(reinterpret_cast<unsigned char*>(req.data()),6);

    stream << lrc;
    Frame.prepend(":");
    Frame.append(req.toHex().toUpper());
    QByteArray reqa;
    reqa.append(Frame);
    COMport->write(reqa+"\r\n");
}

void MainWindow::on_pushButton_Desliga_clicked()
{
    // desliga
    COMport->write(":0105080BFF00E8\r\n");
    QTest::qWait(100);
    COMport->write(":0105080B0000E7\r\n");
    timerCheck->stop();
    ui->label_status->setVisible(false);
    modo=0;
}

void MainWindow::ReadDatatoPlot()
{
    ui->label_status->setText("Lendo dados");
    int m_Addr;
    ponto_memoria=701;
    for(m_Addr=2001;m_Addr<=2701;m_Addr++)
    {
        RequestData(m_Addr);
        QTest::qWait(50);
    }
}

void MainWindow::plotdata(QCustomPlot *customPlot)
{
    customPlot->clearItems();
    customPlot->clearGraphs();

    ui->label_status->setText("Plotando dados");
    ui->label_status->setVisible(true);
    customPlot->addGraph();
    customPlot->graph(0)->setPen(QPen(QColor(0,70,132)));
    customPlot->graph(0)->setBrush(QBrush(QColor(0, 70, 132, 20)));
    QVector<double> x(700), y0(700);
    quint16 freq_local=freq_min;
    impedancia_maxima=0;
    int i;
    for( i=0; i<700; ++i)
    {
        if(impedancia[i]>impedancia_maxima) impedancia_maxima = impedancia[i];
    }
    quint16 freq_max_c, correcao;
    freq_max_c = freq_min + (passo * 700);
    correcao = (freq_max_c - freq_max) / passo;
    qDebug() << correcao;
    for (i=correcao; i<700; ++i)
    {
      x[i] = freq_local;
      freq_local = freq_local + passo;
      y0[i] = (impedancia[i] * 100) / impedancia_maxima;
      //if(freq_local>freq_max) break;
    }
    customPlot->xAxis2->setVisible(true);
    customPlot->xAxis2->setTickLabels(false);
    customPlot->yAxis2->setVisible(true);
    customPlot->yAxis2->setTickLabels(false);
    connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->xAxis2, SLOT(setRange(QCPRange)));
    connect(customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->yAxis2, SLOT(setRange(QCPRange)));
    customPlot->graph(0)->setData(x, y0);
    customPlot->xAxis->setRange(freq_min,freq_max);
    customPlot->yAxis->setRange(0,100);
    //customPlot->graph(0)->rescaleAxes(true);
    //customPlot->setRangeDrag(Qt::Horizontal);
    //customPlot->setRangeZoom(Qt::Horizontal | Qt::Vertical);

    // texto
    textLabel = new QCPItemText(customPlot);
    customPlot->addItem(textLabel);
    textLabel->setPositionAlignment(Qt::AlignTop|Qt::AlignHCenter);
    textLabel->position->setType(QCPItemPosition::ptAxisRectRatio);
    textLabel->position->setCoords(0.5, 0); // place position at center/top of axis rect
    textLabel->setText(ui->lineEdit_freq_res->text()+" Hz");
    textLabel->setFont(QFont(font().family(), 26)); // make font a bit larger
    textLabel->setPen(QPen(Qt::black)); // show black border around text

    /* add the arrow:
    arrow = new QCPItemLine(customPlot);
    customPlot->addItem(arrow);
    arrow->start->setParentAnchor(textLabel->bottom);
    arrow->end->setCoords(ui->lineEdit_freq_res->text().toInt(), ui->lineEdit_imp_min->text().toInt()); // point to (4, 1.6) in x-y-plot coordinates
    arrow->setHead(QCPLineEnding::esSpikeArrow);
    */
    ui->label_status->setVisible(false);
    customPlot->update();
    customPlot->replot();

}

void MainWindow::on_pushButton_1_clicked()
{
    if(ui->radioButton->isChecked())
    {
        ui->lineEdit_freq_min->setText(ui->lineEdit_freq_min->text()+"1");
    } else if(ui->radioButton_2->isChecked())
    {
        ui->lineEdit_freq_max->setText(ui->lineEdit_freq_max->text()+"1");
    }
}

void MainWindow::on_pushButton_2_clicked()
{
    if(ui->radioButton->isChecked())
    {
        ui->lineEdit_freq_min->setText(ui->lineEdit_freq_min->text()+"2");
    } else if(ui->radioButton_2->isChecked())
    {
        ui->lineEdit_freq_max->setText(ui->lineEdit_freq_max->text()+"2");
    }
}

void MainWindow::on_pushButton_3_clicked()
{
    if(ui->radioButton->isChecked())
    {
        ui->lineEdit_freq_min->setText(ui->lineEdit_freq_min->text()+"3");
    } else if(ui->radioButton_2->isChecked())
    {
        ui->lineEdit_freq_max->setText(ui->lineEdit_freq_max->text()+"3");
    }
}

void MainWindow::on_pushButton_4_clicked()
{
    if(ui->radioButton->isChecked())
    {
        ui->lineEdit_freq_min->setText(ui->lineEdit_freq_min->text()+"4");
    } else if(ui->radioButton_2->isChecked())
    {
        ui->lineEdit_freq_max->setText(ui->lineEdit_freq_max->text()+"4");
    }
}

void MainWindow::on_pushButton_5_clicked()
{
    if(ui->radioButton->isChecked())
    {
        ui->lineEdit_freq_min->setText(ui->lineEdit_freq_min->text()+"5");
    } else if(ui->radioButton_2->isChecked())
    {
        ui->lineEdit_freq_max->setText(ui->lineEdit_freq_max->text()+"5");
    }
}

void MainWindow::on_pushButton_6_clicked()
{
    if(ui->radioButton->isChecked())
    {
        ui->lineEdit_freq_min->setText(ui->lineEdit_freq_min->text()+"6");
    } else if(ui->radioButton_2->isChecked())
    {
        ui->lineEdit_freq_max->setText(ui->lineEdit_freq_max->text()+"6");
    }
}

void MainWindow::on_pushButton_7_clicked()
{
    if(ui->radioButton->isChecked())
    {
        ui->lineEdit_freq_min->setText(ui->lineEdit_freq_min->text()+"7");
    } else if(ui->radioButton_2->isChecked())
    {
        ui->lineEdit_freq_max->setText(ui->lineEdit_freq_max->text()+"7");
    }
}

void MainWindow::on_pushButton_8_clicked()
{
    if(ui->radioButton->isChecked())
    {
        ui->lineEdit_freq_min->setText(ui->lineEdit_freq_min->text()+"8");
    } else if(ui->radioButton_2->isChecked())
    {
        ui->lineEdit_freq_max->setText(ui->lineEdit_freq_max->text()+"8");
    }
}

void MainWindow::on_pushButton_9_clicked()
{
    if(ui->radioButton->isChecked())
    {
        ui->lineEdit_freq_min->setText(ui->lineEdit_freq_min->text()+"9");
    } else if(ui->radioButton_2->isChecked())
    {
        ui->lineEdit_freq_max->setText(ui->lineEdit_freq_max->text()+"9");
    }
}

void MainWindow::on_pushButton_0_clicked()
{
    if(ui->radioButton->isChecked())
    {
        ui->lineEdit_freq_min->setText(ui->lineEdit_freq_min->text()+"0");
    } else if(ui->radioButton_2->isChecked())
    {
        ui->lineEdit_freq_max->setText(ui->lineEdit_freq_max->text()+"0");
    }
}

void MainWindow::on_pushButton_limpa_clicked()
{
    if(ui->radioButton->isChecked())
    {
        ui->lineEdit_freq_min->setText("");
    } else if(ui->radioButton_2->isChecked())
    {
        ui->lineEdit_freq_max->setText("");
    }
}
