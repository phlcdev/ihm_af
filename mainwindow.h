#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include <QtCore/QDebug>
#include <QtCore/QTimer>
#include "qextserialenumerator.h"
#include "qextserialport.h"
#include "qdatetime.h"
#include "qtest.h"
#include "3rdparty/qcustomplot/qcustomplot.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void read_serial();
    void timerCheck_timout();
    void timerStatus_timout();
    void on_pushButton_Liga_clicked();
    void on_pushButton_Desliga_clicked();
    void RequestData(quint16 Endereco);
    void plotdata(QCustomPlot *customPlot);
    void ReadDatatoPlot();
    void ReadVars(int cmd);

    void on_pushButton_1_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_8_clicked();

    void on_pushButton_9_clicked();

    void on_pushButton_0_clicked();

    void on_pushButton_limpa_clicked();

private:
    QCPItemText *textLabel;
    QCPItemLine *arrow;
    quint16 freq_max, freq_min, passo;
    quint16 impedancia_maxima;
    void WriteData(quint16 valor, quint16 Endereco);
    int BytesToInt(QByteArray byteArr);
    unsigned char LRC(unsigned char *auchMsg,unsigned short usDataLen);
    Ui::MainWindow *ui;
    QextSerialPort *COMport;
    QTimer *timerCheck;
    QTimer *timerStatus;
    int modo; // 0-idle; 1-scanning; 2-getting data; 4-ressonancia; 5-impedancia
    quint16 impedancia[701];
    int ponto_memoria;
};

#endif // MAINWINDOW_H
